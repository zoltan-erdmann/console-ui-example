// SPDX-License-Identifier: MIT

using imperfect.example.console.ui.cli.generator;

namespace imperfect.example.console.ui.example
{
	[Functionality(name: "add", executable: "Execute", description: "Adds a new item to the collection.")]
	internal class AddItem
	{
		public void Execute([Parameter(description: "Item to add in JSON format.")] string item)
		{
			Console.WriteLine($"Executing add command with parameter: {item}.");
		}
	}
}
