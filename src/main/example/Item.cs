// SPDX-License-Identifier: MIT

using System.ComponentModel;
using System.Runtime.Serialization;

namespace imperfect.example.console.ui.example
{
	[DataObject]
	internal class Item
	{
		[DataMember]
		public string Id { get; }

		[DataMember]
		public string Name { get; }
		
		public Item(string id, string name)
		{
			Id = id;
			Name = name;
		}
	}
}
