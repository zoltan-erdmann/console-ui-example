// SPDX-License-Identifier: MIT

using imperfect.example.console.ui.cli.generator;

namespace imperfect.example.console.ui.example
{
	[Functionality(name: "list", executable: "Execute", description: "List the items from the saved collection.")]
	internal class ListItems
	{
		public void Execute([Option(name: "filter", description:"Filter expression to be used for querying.")] string expression)
		{
			Console.WriteLine($"Executing list command with parameter: {expression}.");
		}
	}
}
