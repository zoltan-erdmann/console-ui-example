// SPDX-License-Identifier: MIT

using imperfect.example.console.ui.cli.generator;

namespace imperfect.example.console.ui.example
{
	[Functionality(name: "remove", executable: "Execute", description: "Removes an item from the collection.")]
	internal class RemoveItem
	{
		public void Execute([Parameter(description:"The ID of the item to be deleted.")] string id)
		{
			Console.WriteLine($"Executing remove command with parameter: {id}.");
		}
	}
}
