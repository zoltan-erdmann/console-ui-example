// SPDX-License-Identifier: MIT

using imperfect.example.console.ui.cli.generator;
using System.Reflection;

namespace imperfect.example.console.ui.cli
{
	internal class Application
	{
		public static int Run(string[] args)
		{
			return Parser.Of(args).Execute()?? PrintShortHelp();
		}

		private static int PrintShortHelp()
		{
			string applicationName = Assembly.GetExecutingAssembly().GetName().Name ?? "<unknown executable>";
			Console.WriteLine($"[Error:argument] Type '{applicationName} help' for more information.");
			return 1;
		}
	}
}
