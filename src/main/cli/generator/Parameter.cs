// SPDX-License-Identifier: MIT

namespace imperfect.example.console.ui.cli.generator
{
	[AttributeUsage(AttributeTargets.Parameter, AllowMultiple = false)]
	public class Parameter : Attribute
	{
		public string Description { get; }

		public Parameter(string description = "")
		{
			Description = description;
		}
	}
}
