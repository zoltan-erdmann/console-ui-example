// SPDX-License-Identifier: MIT

using System.Linq;
using System.Reflection;

namespace imperfect.example.console.ui.cli.generator
{
	internal class Parser
	{
		private string[] args;

		public static Parser Of(string[] args)
		{
			return new Parser(args);
		}

		private Parser(string[] args)
		{
			this.args = args;
		}

		public int? Execute()
		{
			if (args.Length < 1) return null;
			string requestedCommand = args[0];

			var annotatedTypes = GetTypesWithAttribute(Assembly.GetExecutingAssembly(), typeof(Functionality));
			foreach (var annotatedType in annotatedTypes)
			{
				Functionality? functionality = (Functionality?) annotatedType.GetCustomAttribute(typeof(Functionality));
				if (functionality != null && requestedCommand.Equals(functionality.Name, StringComparison.CurrentCultureIgnoreCase))
				{
					//TODO: Quick and dirty...
					if (functionality.Name.Equals("help", StringComparison.CurrentCultureIgnoreCase))
					{
						HelpFunctionality helpFunctionality = new HelpFunctionality();
						helpFunctionality.Execute(annotatedTypes);
						return 0;
					}

					ConstructorInfo? constructor = annotatedType.GetConstructor(Array.Empty<Type>());
					var actionExecutor = constructor?.Invoke(new object[0]);
					MethodInfo? executeMethod = annotatedType.GetMethod(functionality.Executable);
					if(executeMethod != null && actionExecutor != null)
					{
						try
						{
							//TODO: Parse and match parameters.
							executeMethod.Invoke(actionExecutor, new string[] { "Valami üzenet" });
							return 0;
						}
						catch (Exception ex)
						{
							Console.Error.WriteLine($"[Error] Method execution error: {ex}");
							return null;
						}
					}
				}
			}
			return null;
		}

		private IEnumerable<Type> GetTypesWithAttribute(Assembly assembly, Type attributeType)
		{
			foreach (Type type in assembly.GetTypes())
			{
				if (type.GetCustomAttributes(attributeType, true).Length > 0)
				{
					yield return type;
				}
			}
		}
	}
}
