// SPDX-License-Identifier: MIT

namespace imperfect.example.console.ui.cli.generator
{
	[AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
	public class Functionality : Attribute
	{
		public string Name { get; }
		public string Description { get; }
		public string Executable { get; }

		public Functionality(string name, string executable, string description = "")
		{
			Name = name;
			Executable = executable;
			Description = description;
		}
	}
}
