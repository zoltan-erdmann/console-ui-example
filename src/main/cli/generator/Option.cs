// SPDX-License-Identifier: MIT

namespace imperfect.example.console.ui.cli.generator
{
	[AttributeUsage(AttributeTargets.Parameter, AllowMultiple = false)]
	public class Option : Parameter
	{
		public string Name { get; }

		public Option(string name, string description = "")
			: base(description)
		{
			Name = name;
		}
	}
}
