// SPDX-License-Identifier: MIT

namespace imperfect.example.console.ui.cli.generator
{
	[AttributeUsage(AttributeTargets.Parameter, AllowMultiple = false)]
	public class Ignore: Attribute
	{
	}
}
