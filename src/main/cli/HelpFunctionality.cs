// SPDX-License-Identifier: MIT

using imperfect.example.console.ui.cli.generator;
using System.Reflection;

namespace imperfect.example.console.ui.cli
{
	[Functionality(name: "help", executable: "Execute", description: "Shows this help descriptions.")]
	internal class HelpFunctionality
	{
		public void Execute([Ignore] IEnumerable<Type> types)
		{
			PrintUsageInformation();
			Console.WriteLine();
			PrintCommands(types);
		}

		private void PrintUsageInformation()
		{
			string applicationName = Assembly.GetExecutingAssembly().GetName().Name ?? "<unknown executable>";
			Console.WriteLine($"Usage: {applicationName} <command> [options]");
		}

		private void PrintCommands(IEnumerable<Type> types)
		{
			if (types.Count() == 0) return;
			Console.WriteLine("Commands:");
			foreach (Type type in types)
			{
				Functionality? functionality = (Functionality?)type.GetCustomAttribute(typeof(Functionality));
				PrintCommand(type, functionality);
			}
		}
		private void PrintCommand(Type type, Functionality? functionality)
		{
			if (functionality == null) return;
			Console.WriteLine($"  {functionality.Name} - {functionality.Description}");
			PrintArgument(type.GetMethod(functionality.Executable));
			Console.WriteLine();
		}

		private void PrintArgument(MethodInfo? method)
		{
			if (method == null || method.GetParameters().Where(HasNotIgnoredArgument).Count() == 0) return;
			Console.WriteLine("  Options:");
			foreach (ParameterInfo parameterInfo in method.GetParameters())
			{
				Option? option = (Option?)parameterInfo.GetCustomAttribute(typeof(Option));
				Parameter? parameter = (Parameter?)parameterInfo.GetCustomAttribute(typeof(Parameter));
				Ignore? ignoredParameter = (Ignore?)parameterInfo.GetCustomAttribute(typeof(Ignore));
				if (option != null)
				{
					Console.WriteLine($"    --{option.Name} - {option.Description}");
				}
				else if (parameter != null)
				{
					Console.WriteLine($"    [{parameterInfo.Name}] - {parameter.Description}");
				}
				else if (ignoredParameter == null)
				{
					Console.WriteLine($"    [{parameterInfo.Name}] - [Unknown parameter.]");
				}
			}
		}

		private bool HasNotIgnoredArgument(ParameterInfo parameterInfo)
		{
			return parameterInfo.GetCustomAttribute(typeof(Ignore)) == null;
		}
	}
}
