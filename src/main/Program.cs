// SPDX-License-Identifier: MIT

using imperfect.example.console.ui.cli;

namespace imperfect.example.console.ui
{
	class Program
	{
		public static int Main(string[] args)
		{
			return Application.Run(args);
		}
	}
}
